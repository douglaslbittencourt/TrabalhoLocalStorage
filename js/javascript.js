/**
 * Carrega os dados iniciais no localStorage
 */
function onLoadLocalStorage() {
    if (!localStorage.getItem("itens")) {
        var json = '[{"nome" : "teste", "unidade" : "2", "quantidade" : "1", "preco" : "12", "perecivel" : "true", "validade" : "15/12/1997", "fabricacao" : "15/12/1997"},' +
            '{"nome" : "teste", "unidade" : "2", "quantidade" : "1", "preco" : "12", "perecivel" : "true", "validade" : "15/12/1997", "fabricacao" : "15/12/1997"}]';

    localStorage.setItem("itens", json);
    }
}

/**
 * Carrega os dados do localStorage na table
 */
function onLoadCreateTableFromjson() {
    var col = [],
        json = JSON.parse(localStorage.getItem("itens"));

    console.log(json);
    //Carrega o Json no array
    for (var i = 0; i < json.length; i++) {
        for (var key in json[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }

    //Cria a tabela e adiciona suas classes
    var table = document.createElement("table");
    table.classList.add("table");
    table.classList.add("table-striped");


    var tr = table.insertRow(-1);


    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");
        th.innerHTML = col[i];
        tr.appendChild(th);
    }
    var t = document.createElement("th");
    t.innerHTML = "Ações";
    tr.appendChild(t);

    for (var i = 0; i < json.length; i++) {
        tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            if(j == 0){
                tabCell.classList.add(".nr");
            }
            tabCell.innerHTML = json[i][col[j]];
        }
        //Criando Ações na consulta
        var Cell = tr.insertCell(-1);
        buttonDelete = document.createElement("button");
        // button.innerHTML = "Deletar"
        buttonDelete.type = "button";
        buttonDelete.classList.add("btn");
        buttonDelete.classList.add("btn-danger");
        buttonDelete.classList.add("fas");
        buttonDelete.classList.add("fa-times");
        Cell.appendChild(buttonDelete);

        buttonEdit = document.createElement("button");
        buttonEdit.type = "button";
        buttonEdit.classList.add("btn");
        buttonEdit.classList.add("btn-success");
        buttonEdit.classList.add("fas");
        buttonEdit.classList.add("fa-pencil-alt");
        buttonEdit.classList.add("clique");
        Cell.appendChild(buttonEdit);
    }

    //limpa a div e adiciona a tabela
    var divContainer = document.getElementById("table");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
}


function onSubmitInsertJason() {
    var nome       = document.getElementById("nome"),
        unidade    = document.getElementById("unidade"),
        quantidade = document.getElementById("quantidade"),
        preco      = document.getElementById("preco"),
        perecivel  = document.getElementById("perecivel"),
        valildade  = document.getElementById("validade"),
        fabricacao = document.getElementById("fabricacao"),
        dados      = `{"nome" : " ${nome.value}", "unidade" : " ${unidade.value}", "quantidade" : " ${quantidade.value}","preco" : " ${preco.value}", "perecivel" : " ${((perecivel.value === 'on') ? 'true' : 'false')}", "validade" : " ${reformatDateString(valildade.value)}", "fabricacao" : " ${reformatDateString(fabricacao.value)}"}`,
        json       = JSON.parse(localStorage.getItem("itens"));

    if(nome.value && unidade.value && preco.value && fabricacao.value){
        json.push(JSON.parse(dados));
        localStorage.setItem("itens", JSON.stringify(json));
    }
}

function reformatDateString(s) {
    var b = s.split(/\D/);
    return b.reverse().join("\/");
}
